#!/pro/bin/perl

use strict;
use warnings;
use DBI;

my $dbh = DBI->connect ("dbi:mysql:database=ts_db", undef, undef, {
    RaiseError         => 1,
    PrintError         => 1,
    AutoCommit         => 1,
    ChopBlanks         => 1,
    ShowErrorStatement => 1,
    }) or die $DBI::errstr;

my $sth = $dbh->prepare ("select rid, report from reports");
   $sth->execute;
   $sth->bind_columns (\my ($rid, $report));
my %hst;
while ($sth->fetch) {
    $report or next;

    $report =~ s/\r\n?/\n/g;
    $report =~ s/\s*\n(?:\s*\n)+\s*/\n/g;
    $report =~ m/Automated smoke report for.*\n\s*(\S+):/i or next;
    $hst{$rid} = $1;
    }

my $sts = $dbh->prepare ("update reports set hostname = ? where rid = ?");
$sts->execute ($hst{$_}, $_) for keys %hst;
$sts->finish;

$dbh->disconnect;
