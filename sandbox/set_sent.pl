#!/pro/bin/perl

use strict;
use warnings;
use autodie;

use DBI;

my $dbc = DBI->connect ("dbi:CSV:", undef, undef, {
    RaiseError	=> 1,
    PrintError	=> 1,
    ChopBlanks	=> 1,
    f_dir	=> ".",
    f_ext	=> ".csv/r",
    });
my $dbm = DBI->connect ("dbi:mysql:database=ts_db", undef, undef, {
    RaiseError	=> 1,
    PrintError	=> 1,
    ChopBlanks	=> 1,
    AutoCommit	=> 1,
    });

my $has_sent;
eval {
    my $sth = $dbm->prepare ("select * from reports");
       $sth->execute;
    ($has_sent) = grep { $_ eq "sent" } @{$sth->{NAME_lc}};
    };

$has_sent or
    $dbm->do ("alter table reports add column sent smallint default 0");

my $stu = $dbm->prepare ("update reports set sent = 1 where rid = ?");
my $sth = $dbc->prepare ("select rid from rids_sent");
   $sth->execute;
   $sth->bind_columns (\my $rid);
while ($sth->fetch) {
    $stu->execute ($rid);
    }
$stu->finish;
