#!/pro/bin/perl

use strict;
use warnings;

sub usage
{
    my $err = shift and select STDERR;
    print "usage: $0 [--send]\n";
    exit $err;
    } # usage

use Getopt::Long qw(:config bundling nopermute);
my $opt_s = 0;
my $opt_t = 0;
my $opt_n = 0;
my $opt_v = 0;
# todo: options to send a range of report id's
GetOptions (
    "help|?"		=> sub { usage (0); },

    "s|send!"		=> \$opt_s,
    "t|test"		=> \$opt_t,
    "n|nth=i"		=> \$opt_n,
    "v|verbose:1"	=> \$opt_v,
    ) or usage (1);

use DBI;
use JSON;
use Data::Peek;
use Time::HiRes qw( gettimeofday tv_interval );
use Test::Smoke::Metabase;

my %rec;

sub host_from_report
{
    $rec{rpt_header} =~ m/automated smoke.*\n(\w+):/i ? $1 : "?";
    } # host_from_report

sub parallel_from_report
{
    $rec{report} =~ m/harness[-_ ]options.*j[1-9]/i ? 1 : 0;
    } # parallel_from_report

my %weight = qw( ? -1 - 0 O 1 X 2 F 3 t 4 M 5 m 6 c 7 );

sub go
{
    my $nth = shift // 0;
    my %count;

    my $dbh = DBI->connect ("dbi:mysql:database=ts_db", undef, undef, {
	RaiseError         => 1,
	PrintError         => 1,
	AutoCommit         => 1,
	ChopBlanks         => 1,
	ShowErrorStatement => 1,
	FetchHashKeyName   => "NAME_lc",
	}) or die $DBI::errstr;

    my $sts = $dbh->prepare ("update reports set sent = 1 where rid = ?");
    my $sel = $opt_s ? "where sent = 0" : "";
    my $sth = $dbh->prepare ("select * from reports $sel order by rid desc");
       $sth->execute;
       $sth->bind_columns (\@rec{@{$sth->{NAME_lc}}});
    REPORT: while ($sth->fetch) {

#	$rec{rid} == 76293 or next;
	if ($opt_n) {
	    $rec{rid} % $opt_n == $nth or next;
	    }

	$rec{report} or next;

	$count{accepted}++;

	my $t0 = [ gettimeofday ];

	$rec{report} =~ s/\r\n?/\n/g;
	$rec{report} =~ s/\s*\n(?:\s*\n)+\s*/\n\n/g;
	$rec{report} =~ m/^[\s\n]*(.*?)\n(?:\s*?\n)+/s and
	    $rec{rpt_header} = $1;

	$rec{plevel} =~ m/^[0-9a-f]+$/ or next;
	$count{with_plevel}++;

	my $id_hash = {
	    smoke_date		=> $rec{dtsmoke},
	    perl_id		=> $rec{pversion},
	    git_id		=> $rec{plevel},
	    applied_patches	=> "?",
	    };
	my $node_hash = {
	    hostname		=> $rec{hostname} || host_from_report,
	    architecture	=> $rec{arch},
	    osname		=> $rec{os},
	    osversion		=> $rec{osver},
	    cc			=> $rec{cc},
	    ccversion		=> $rec{ccversion},
	    user		=> $rec{smoker},
	    };
	my $build_hash = {
	    TEST_JOBS		=> "?",
	    LC_ALL		=> "?",
	    LANG		=> "?",
	    manifest_msgs	=> "",
	    compiler_msgs	=> "",
	    skipped_tests	=> "-",
	    harness_only	=> 0,
	    summary		=> "?",
	    };

	my $parallel = parallel_from_report;
	my $output   = $rec{report}; $rec{report} = "";
	my @report   = grep m/\S/ => split m/\n\n/ => $output, -1;

	# Weed out the unused blocks. Data already in database
	foreach my $r (0 .. $#report) {
	    $report[$r] =~ m/^Automated\s+smoke\s+report\s+for\s+/i or next;
	    splice @report, $r, 1;
	    last;
	    }

	foreach my $r (0 .. $#report) {
	    $report[$r] =~ m/^Summary:\s*\S+$/ or next;
	    splice @report, $r, 1;
	    last;
	    }

	foreach my $r (0 .. $#report) {
	    $report[$r] =~ m/^O = OK  F = Failure\(s\)/ or next;
	    splice @report, $r, 1;
	    last;
	    }

	# Fetch single-item information
	foreach my $r (0 .. $#report) {
	    $report[$r] =~ s/^Tests\s+skipped\s+on\s+user\s+request\s*:[\s\n]+
		( (?:\s+.*\n)+ )//ix or next;
	    ($build_hash->{skipped_tests} = $1) =~ s/\s+$//;
	    last;
	    }

	foreach my $r (0 .. $#report) {
	    $report[$r] =~ s/^Testsuite\s+was\s+run\s+only\s+with\s+'?harness'?\s*\n*//i or next;
	    $build_hash->{harness_only} = 1;
	    $report[$r] =~ m/\S/ or splice @report, $r, 1;
	    last;
	    }

	foreach my $r (0 .. $#report) {
	    $report[$r] =~ s/^Locally\s+applied\s+patches:?[\s\n]*//i or next;
	    my $lap = splice @report, $r, 1;
	    $id_hash->{applied_patches} = join " ", grep m/\S/ =>
		grep { !m/^SMOKE$rec{plevel}/ } split m/[\s\n]+/ => $lap;
	    last;
	    }

	foreach my $r (0 .. $#report) {
	    $report[$r] =~ m/^--[\s\n]+Report by Test::Smoke/ or next;
	    splice @report, $r, 1;
	    last;
	    }

	foreach my $r (0 .. $#report) {
	    $report[$r] =~ s/^Compiler\s+messages[\s\n]*//i or next;
	    my $cmm = splice @report, $r, 1;
	    $build_hash->{compiler_msgs} = join "\n",
		grep m/\S/ => split m/\s*\n+/ => $cmm;
	    last;
	    }
	#DDumper[$id_hash,$node_hash,$build_hash,@report];

	my ($has_dbg, @conf_lines, @conf_x, $common) = (0);
	foreach my $r (0 .. $#report) {
	    $report[$r] =~ m/^(?:\S+\s+)?Configuration/ or next;
	    my $conf_block = splice @report, $r, 1;
	    @conf_lines = split m/\s*\n/ => $conf_block;
	    $common = $conf_lines[0] =~ m/\(common\)\s*(.*\S)/ ? $1 : "";
	    $common eq "none" and $common = "";
	    while ($conf_lines[-1] =~ m/^(?:\|\s+|\+)+-+\s+(.*)/) {
		my $c = $1;
		pop @conf_lines;
		$c =~ m/^(?:PERLIO\s*=\s*(\S+)\s*)?(.*?)\s*(-DD?EBUGGING)?\s*$/;
		push @conf_x, [ $1, $2, undef, $3 ? 1 : 0 ];
		$conf_x[-1][3] and $has_dbg = 1;
		$conf_x[-1][1] =~ s/^LC_ALL\s*=\s*(\S+)\s*// and
		    ($conf_x[-1][0], $conf_x[-1][2]) = ("locale", $1);
		$conf_x[-1][1] eq "no debugging" and
		    $conf_x[-1][1] = ""; # rid = 80127
		$conf_x[-1][0] ||= "perlio";
		}
	    last;
	    }
	@conf_lines or
	    die "Eacks, no conf block in $rec{rid}:\n---\n$rec{rpt_header}\n---\n";

	$count{process}++;
	$count{"conf_".scalar@conf_lines}++;

	my $full_output = join "\n\n" => @report;

	# 80088 en 80299 <= moeten goed gaan
	#$rec{rid} == 80088 and die DDumper [ \%rec, \@conf_lines, \@report ];
	my $now = scalar localtime;
	foreach my $cl (@conf_lines) {
	    $cl .= " ";
	    foreach my $dbg (0 .. $has_dbg) {
		printf STDERR "%3d %6d %6d/%6d %s prv: %5.2f all: %5.2f\r",
		    $nth, $rec{rid}, $count{process}, $count{sent}, $now,
		    $count{t_prv} // 0, ($count{t_all} // 0) / ($count{done} // 1);

		$build_hash->{summary} = "?";
		$cl =~ m/^((?:[-OFXMm?c]\s+)+)(.*)/ or next;
		my ($cfg, @summ) = ($2, split m/\s+/ => $1);
		@summ == @conf_x or next REPORT; # conf mismatch

		$count{"summ_".scalar@conf_lines}++;

		# TODO: break up full_output into matching parts

		my @result;
		foreach my $i (0 .. $#summ) {
		    $conf_x[$i][3] == $dbg or next;

		    $weight{$summ[$i]} > $weight{$build_hash->{summary}} and
			$build_hash->{summary} = $summ[$i];

		    push @result, {
			io_env	=> $conf_x[$i][0],
			output	=> $full_output,
			summary	=> $summ[$i],
			locale	=> $conf_x[$i][2],
			};
		    }

		my $report = Test::Smoke::Metabase->open (
		    resource     => "perl:///commit/$rec{plevel}",
		    );

		$report->add ("Test::Smoke::Fact::SmokeID" => $id_hash);
		$report->add ("Test::Smoke::Fact::Node"    => $node_hash);
		$report->add ("Test::Smoke::Fact::Build"   => $build_hash);

		$report->add ("Test::Smoke::Fact::Config"  => {
		    parallel  => $parallel,
		    arguments =>
			join " ", grep m/\S/ => $common, $cfg,
			    $dbg ? "-DDEBUGGING" : "",
		    });

		$report->add ("Test::Smoke::Fact::Result" => $_) for @result;

		$report->close ();

		if ($opt_t) {
		    $opt_v and DDumper ($report);
		    JSON->new->ascii->encode ($report->as_struct);
		    }
		elsif ($opt_s && !$rec{sent}) {
		    $opt_v and DDumper ($report);
		    $report->send ({
			uri     => "http://metabase.cpantesters.org/beta/",
			id_file => "tux.json",
			});
		    $count{sent}++;
		    }
		$count{done}++;
		$count{t_prv}  = tv_interval ($t0);
		$count{t_all} += $count{t_prv};
		$count{t_avg}  = $count{t_all} / $count{done};
		}
	    }
	$opt_s and $sts->execute ($rec{rid});
	}

    $_->finish for $sth, $sts;
    $dbh->disconnect;

    DDumper\%count;
    } # go

if ($opt_n) {
    foreach my $x (1 .. $opt_n) {
	fork and go ($x - 1);
	}

    use POSIX ":sys_wait_h";
    do { sleep 1 } while waitpid (-1, WNOHANG);
    }
else {
    go ();
    }
